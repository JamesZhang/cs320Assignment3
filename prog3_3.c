#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define ALPHABET_LEN 26
#define UPPER_START 'A'
#define LOWER_START 'a'

void caesar(int shift) // perform Caesar shift on letters from stdin
{
  char c, u; // c=character to encode/encrypt, u=uppercase
  int b; // is c uppercase? ("boolean")
  int i; // letter number of character

  // Make sure that shift is in the range from 0 to ALPHABET_LEN-1,
  // by first modding it by (taking the remainder modulo) ALPHABET_LEN,
  // which puts it in the range from -ALPHABET_LEN+1 to ALPHABET_LEN-1;
  // then adding ALPHABET_LEN to ensure the result is positive;
  // and finally taking the remainder again.
  // This results in a quantity that is congruent to the original
  // modulo ALPHABET_LEN but always non-negative, in the desired range.
  //
  shift = ((shift % ALPHABET_LEN)
                  + ALPHABET_LEN)
                  % ALPHABET_LEN;

  while ((c = fgetc(stdin)) != EOF) // read char from stdin
  {
    u = toupper(c);
    b = (u == c) ? 1 : 0;
    i = u - UPPER_START + 1; // letter number A=1, ..., Z=26
    if (i > 0 && i <= ALPHABET_LEN)
    {
      i = (i - 1 + shift) % ALPHABET_LEN; // shift in range 0-25 (non-negative!)
      c = (b ? UPPER_START : LOWER_START) + i; // start + shift is correct
    }
    putchar(c); // write character to stdout
  }
}

int main(int argc, char** argv)
{
  int shift;
  if (argc != 2) {
    fprintf(stderr,"%s: expected one argument (the integer shift)\n", argv[0]);
    exit(1);
  }
  shift = atoi(argv[1]);

  caesar(shift);

  return 0;
}

