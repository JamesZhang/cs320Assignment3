#!/bin/bash

# header:
echo Assignment \#3-3, James Zhang, zhangjames389@gmail.com

if [ ! -f prog3_3 ] ; then
  gcc -o prog3_3 prog3_3.c
fi


T=`date +%Y%m%d.%H%M%S`
echo "#!/bin/bash" > prog3_3.$T.sh
echo "#$T" >> prog3_3.$T.sh

find . -type f -name '*.enc' \
  -exec echo -n "cat {}|./prog3_3 $1 >" \; \
  -exec basename -z -s .enc {} .dec \; \
  -exec echo "" \; \
  |tee -a prog3_3.$T.sh \
  |sed -e 's/.*>//'

sh prog3_3.$T.sh

