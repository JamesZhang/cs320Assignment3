#!/bin/bash

# header:
echo Assignment \#3-1, James Zhang, zhangjames389@gmail.com

# arg 1: Logins
# arg 2: sd.lindeneau.com
# arg 3: YOKO JAMIKA

# Tasks:
# Split name into first, last (we only need first name for login)
# This script will obtain Yoko's password from the provided source (argument 1).
# It will then copy all of the files in Yoko's home directory on the target server
# (argument 2) with the .enc extension, in addition to the encryption executable:
# encryptor to your current directory.

# get regular expression version of name:
E=`echo $3|sed 's/\s/\\\\s/'`; #  echo "Name (regular expression): $E"

# get login & password from Logins:
L=`grep -ie $E $1|cut -d, -f1|sed -e 's/YOKO JAMIKA/yoko/'`; # echo "Login: $L"
P=`grep -ie $E $1|cut -d, -f3`; #  echo "Password: $P"

USERNAME=$L
TARGET=$2
PASSWORD=$P

# yes/no, timeout: http://www.tamas.io/automatic-scp-using-expect-and-bash/
expect -c "
  log_user 0
  set timeout 1
  spawn scp $USERNAME@$TARGET:\{encryptor,\*.enc\} .
  expect yes/no { send yes\r ; exp_continue }
  expect password: { send $PASSWORD\r }
  sleep 1
  exit
"
if [ -f "./encryptor" ]; then
  echo "encryptor"
fi

for i in *.enc; do
  echo $i 
done


