James Zhang
zhangjames389@gmail.com

The third homework assignment for CS320 Fall2016.
This assignment is intended to get the user accustomed c and bash working in conjunction

The user shall, in less than reputable methods, shall steal encrypted files from a competitor company and decode them for their employer to use.
This assignment consists of three scripts and two C programs which with methodically steal, solve the encryption pattern, and decrypt the files.



**** All files must be made executable before running *****
**** To make executable type into terminal: chmod +x "insert file name" ****



prog3_1.sh:
A script which takes in three arguments to login and steal the files.
It obtains the competitor employee's password, accesses a target server, and then steals the encrypted files from their directory.

Ran as: ./prog3_1.sh Logins sd.lindeneau.com "Yoko Jamika"

prog3_2.c:
A program that breaks the encryption of the files. 
Taking in ciphertext on a command line argument and plaintext it calculates the offset between them, giving the thief the "key". 

prog3_2.sh:
A script which calls the encryptor program with plaintext of the user's choice as well as the C program and gives the difference between them.
Additionally, it automatically compiles the c program it calls if not already compiled.
Ran as: ./prog3_2.sh


prog3_3.c:
Implements the Caesar Cipher and takes in a command line argument of the offset discovered earlier and encrypts/decrypts input given from standard in.
It only applies to letters and ignores all other characters.

prog3_3.sh:
Takes in a command line argument of the offset discovered by prog3_2.sh and then uses the previous c program to decrypt all of the encrypted files.
While decrypting it echos a .dec file for each file that has been decrypted.
Additionally, it automatically compiles the c program it calls if not already compiled.
Ran as: ./prog3_3.sh (offset value) i.e if prog3_2.sh outputs 6 then it's ran as ./prog3_3.sh 6
