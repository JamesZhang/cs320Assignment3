#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define ALPHABET_LEN 26
#define ALPHABET_START 'A'

// Two compilation targets are possible:
// gcc -DDEBUG=1 -o prog3_2 prog3_2.c # (for debugging; "=1" optional)
// gcc -DDEBUG=0 -o prog3_2 prog3_2.c # (default)
#ifndef DEBUG
#define DEBUG 0
#endif

int nextAlphaChar() // get next alphabet character from stdin
{
  int c;
  char cypher;
  while ((cypher = fgetc(stdin)) != EOF)
  {
    c = toupper(cypher) - ALPHABET_START + 1; // letter number A=1, ..., Z=26
    if (c < 1 || c>ALPHABET_LEN) continue;
    return c; // returns in the range 1-26
  }
  return 0; // returns outside the range 1-26, to generate a bogus difference
}

int main(int argc, char** argv)
{
  char plain, cypher, *s;
  int  i, j, p, c, diff, lastdiff;
  // i = letter/comparison count/number (from command line)
  // j = argument count/number from command line

  // debugging "header" line
  if (DEBUG)
    printf("%4s%8s%8s%8s%8s%6s\n","char","plain","cypher","plain","cypher","diff");
  for (i=j=0; ++j<argc; ) // j is index into which string from argv to process
  {
    for (s=argv[j]; (plain = *s) != '\0'; s++) // s is pointer to chars in argv[j]
    {
      // get the next letter character (from the next command line argument)
      plain = toupper(plain); // convert character to uppercase
      p = plain - ALPHABET_START + 1; // calculate the letter number: A=1, ..., Z=26
      if (p < 1 || p>ALPHABET_LEN) continue; // skip non-letters

      // get the next letter number from stdin (cypher character stream)
      c = nextAlphaChar(); // get next cyphertext letter (number) from stdin
      //if (c==0) exit 1; // should we break/exit if there was no next letter from stdin?
      cypher = ALPHABET_START + c - 1; // reconstruct the character from the letter number

      // calculate the letter difference between the alphabetic plain and cypher character pair
      diff = (ALPHABET_LEN + c - p) % ALPHABET_LEN;

      // print first diff, and any subsequent diffs that differ from the last encountered diff
      if ((i++ == 0) || (lastdiff != diff))
        if (!DEBUG)
           printf("%d\n",diff);
      lastdiff = diff;

      // debugging "data" line
      if (DEBUG)
        printf("%4d%8c%8c%8d%8d%6d\n", i, plain, cypher, p, c, diff);
    }
  }

  return 0;
}

